package chess;

import java.util.List;

/**
 * The abstract class that will be extended and fully implemented by each specific chess piece type.
 *
 * @author Micky Aflalo
 * @author Oren Mendelow
 */
public abstract class ChessPiece {
    /**
     * currentLocation allows the ChessPiece to keep track of where on the board it is
     */
    private ChessPieceLocation currentLocation;

    /**
     * color keeps track of which team the piece is on
     */
    private final ChessPieceColor color;

    /**
     * hasBeenMoved keeps track of whether the piece has moved yet during the current game, important for pieces such
     * as the pawn, king, and rook and in moves such as enpassant, castling, and a pawn's initial move
     */
    protected boolean hasBeenMoved;

    /**
     * Super constructor that creates a new chess piece.
     *
     * @param board           The board that we are going to put the piece on.
     * @param color           The color of the piece we are going to create.
     * @param initialLocation The location that the piece will be put in.
     */
    public ChessPiece(ChessBoard board, ChessPieceColor color, ChessPieceLocation initialLocation) {
        this.board = board;
        this.color = color;
        this.currentLocation = initialLocation;
        this.hasBeenMoved = false;
    }

    /**
     * The ChessBoard object on which the game is played and the {@link ChessPiece}'s are stored and updated.
     */
    private final ChessBoard board;

    /**
     * Fully implemented, this method computes, stores, and returns all of the legal moves the current piece can make given its exact location.
     *
     * @return Returns a list with all the possible moves that the current piece can make.
     */
    public abstract List<ChessPieceLocation> getPossibleMoves();

    /**
     * Fully implemented, this method will return the id of the current piece.
     *
     * @return Returns the id of the current piece as a String.
     */
    public abstract String getPieceID();

    /**
     * This method will return the color of the current Piece.
     *
     * @return Returns the color of the current chess piece.
     */
    protected ChessPieceColor getColor() {
        return this.color;
    }

    /**
     * This method will return the exact location of the current piece on the board.
     *
     * @return Returns the exact location of the current piece on the board.
     */
    public ChessPieceLocation getCurrentLocation() {
        return this.currentLocation;
    }

    /**
     * This method will set the current location of the current piece.
     *
     * @param currentLocation The location that the current piece will be moved/set to.
     */
    public void setCurrentLocation(ChessPieceLocation currentLocation) {
        this.currentLocation = currentLocation;
    }

    /**
     * This method will determine if the desired location to move the piece is legal and what kind of move is it.
     * If the move is invalid, empty, or capture.
     *
     * @param location The location that the current piece will be moved/set to.
     * @return Returns the validity of the move: whether the location is empty, an invalid move, or an enemy piece capture.
     */
    protected ChessMoveValidity isValidMove(ChessPieceLocation location) {
        if (!location.isValid()) {
            return ChessMoveValidity.INVALID;
        }
        ChessPiece pieceAtLocation = ChessBoard.getPieceAtLocation(location);
        //If the spot is not empty
        if (pieceAtLocation != null) {
            //If they are on the SAME_TEAM return invalid
            if (pieceAtLocation.getColor() == this.color) {
                return ChessMoveValidity.INVALID;
            }
            return ChessMoveValidity.CAPTURE;
        }
        return ChessMoveValidity.EMPTY;
    }
}
