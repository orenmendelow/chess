package chess.pieces;

import chess.*;

import java.util.ArrayList;
import java.util.List;

/**
 * This class extends the abstract {@link ChessPiece} class to define and implement a Rook chess piece.
 *
 * @author Micky Aflalo
 * @author Oren Mendelow
 */
public class Rook extends ChessPiece {

    /**
     * The id of the current piece.
     */
    private final String pieceID;

    /**
     * Constructor that creates a new Rook chess piece.
     *
     * @param board           The board that we are going to put the piece on.
     * @param color           The color of the piece we are going to create.
     * @param initialLocation The location that the piece will be put in.
     */
    public Rook(ChessBoard board, ChessPieceColor color, ChessPieceLocation initialLocation) {
        super(board, color, initialLocation);
        this.pieceID = color == ChessPieceColor.WHITE ? "wR" : "bR";
    }

    /**
     * This method computes, stores, and returns all of the legal moves the current piece can make given its exact location.
     *
     * @return Returns a list with all the possible moves that the current piece can make.
     */
    @Override
    public List<ChessPieceLocation> getPossibleMoves() {
        List<ChessPieceLocation> possibleMoves = new ArrayList<>();

        int currentColumn = getCurrentLocation().getColumn();
        int currentRow = getCurrentLocation().getRow();


        //Can I move to the RIGHT from where I am at?
        for (int i = currentColumn + 1; i < 8; i++) {
            ChessPieceLocation moveRight = new ChessPieceLocation(i, currentRow);
            if (isValidMove(moveRight) != ChessMoveValidity.INVALID) {
                if (isValidMove(moveRight) != ChessMoveValidity.EMPTY) {
                    possibleMoves.add(moveRight);
                    break;
                }
                possibleMoves.add(moveRight);
            } else {
                break;
            }
        }

        //Can I move to the LEFT from where I am at?
        for (int i = currentColumn - 1; i >= 0; i--) {
            ChessPieceLocation moveLeft = new ChessPieceLocation(i, currentRow);
            if (isValidMove(moveLeft) != ChessMoveValidity.INVALID) {
                if (isValidMove(moveLeft) != ChessMoveValidity.EMPTY) {
                    possibleMoves.add(moveLeft);
                    break;
                }
                possibleMoves.add(moveLeft);
            } else {
                break;
            }
        }

        //Can I move to the UP from where I am at?
        for (int i = currentRow + 1; i < 8; i++) {
            ChessPieceLocation moveUp = new ChessPieceLocation(currentColumn, i);
            if (isValidMove(moveUp) != ChessMoveValidity.INVALID) {
                if (isValidMove(moveUp) != ChessMoveValidity.EMPTY) {
                    possibleMoves.add(moveUp);
                    break;
                }
                possibleMoves.add(moveUp);
            } else {
                break;
            }
        }

        //Can I move to the Down from where I am at?
        for (int i = currentRow - 1; i >= 0; i--) {
            ChessPieceLocation moveDown = new ChessPieceLocation(currentColumn, i);
            if (isValidMove(moveDown) != ChessMoveValidity.INVALID) {
                if (isValidMove(moveDown) != ChessMoveValidity.EMPTY) {
                    possibleMoves.add(moveDown);
                    break;
                }
                possibleMoves.add(moveDown);
            } else {
                break;
            }
        }

        return possibleMoves;
    }

    /**
     * This method will return the id of the current piece.
     *
     * @return The id of the current piece.
     */
    @Override
    public String getPieceID() {
        return this.pieceID;
    }
}
