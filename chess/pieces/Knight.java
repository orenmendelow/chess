package chess.pieces;

import chess.*;

import java.util.ArrayList;
import java.util.List;

/**
 * This class extends the abstract {@link ChessPiece} class to define and implement a Knight chess piece.
 *
 * @author Micky Aflalo
 * @author Oren Mendelow
 */
public class Knight extends ChessPiece {

    /**
     * The id of the current piece.
     */
    private final String pieceID;

    /**
     * Constructor that creates a new Knight chess piece.
     *
     * @param board           The board that we are going to put the piece on.
     * @param color           The color of the piece we are going to create.
     * @param initialLocation The location that the piece will be put in.
     */
    public Knight(ChessBoard board, ChessPieceColor color, ChessPieceLocation initialLocation) {
        super(board, color, initialLocation);
        this.pieceID = color == ChessPieceColor.WHITE ? "wN" : "bN";
    }

    /**
     * This method computes, stores, and returns all of the legal moves the current piece can make given its exact location.
     *
     * @return Returns a list with all the possible moves that the current piece can make.
     */
    @Override
    public List<ChessPieceLocation> getPossibleMoves() {
        List<ChessPieceLocation> possibleMoves = new ArrayList<>();

        int currentColumn = getCurrentLocation().getColumn();
        int currentRow = getCurrentLocation().getRow();

        //COL: L = left, R = Right
        //ROW: U = Up, D = down
        //These go in a clockwise motion starting from left2up1
        ChessPieceLocation left2up1 = new ChessPieceLocation(currentColumn - 2, currentRow + 1);
        if (isValidMove(left2up1) != ChessMoveValidity.INVALID) {
            possibleMoves.add(left2up1);
        }

        ChessPieceLocation left1up2 = new ChessPieceLocation(currentColumn - 1, currentRow + 2);
        if (isValidMove(left1up2) != ChessMoveValidity.INVALID) {
            possibleMoves.add(left1up2);
        }

        ChessPieceLocation right1up2 = new ChessPieceLocation(currentColumn + 1, currentRow + 2);
        if (isValidMove(right1up2) != ChessMoveValidity.INVALID) {
            possibleMoves.add(right1up2);
        }

        ChessPieceLocation right2up1 = new ChessPieceLocation(currentColumn + 2, currentRow + 1);
        if (isValidMove(right2up1) != ChessMoveValidity.INVALID) {
            possibleMoves.add(right2up1);
        }

        ChessPieceLocation right2down1 = new ChessPieceLocation(currentColumn + 2, currentRow - 1);
        if (isValidMove(right2down1) != ChessMoveValidity.INVALID) {
            possibleMoves.add(right2down1);
        }

        ChessPieceLocation right1down2 = new ChessPieceLocation(currentColumn + 1, currentRow - 2);
        if (isValidMove(right1down2) != ChessMoveValidity.INVALID) {
            possibleMoves.add(right1down2);
        }

        ChessPieceLocation left1down2 = new ChessPieceLocation(currentColumn - 1, currentRow - 2);
        if (isValidMove(left1down2) != ChessMoveValidity.INVALID) {
            possibleMoves.add(left1down2);
        }

        ChessPieceLocation left2down1 = new ChessPieceLocation(currentColumn - 2, currentRow - 1);
        if (isValidMove(left2down1) != ChessMoveValidity.INVALID) {
            possibleMoves.add(left2down1);
        }

        return possibleMoves;
    }

    /**
     * This method will return the id of the current piece.
     *
     * @return The id of the current piece.
     */
    @Override
    public String getPieceID() {
        return this.pieceID;
    }
}
