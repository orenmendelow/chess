package chess.pieces;

import chess.*;

import java.util.ArrayList;
import java.util.List;

/**
 * This class extends the abstract {@link ChessPiece} class to define and implement a Bishop chess piece.
 *
 * @author Micky Aflalo
 * @author Oren Mendelow
 */
public class Bishop extends ChessPiece {

    /**
     * The id of the current piece.
     */
    private final String pieceID;

    /**
     * Constructor that creates a new Bishop chess piece.
     *
     * @param board           The board that we are going to put the piece on.
     * @param color           The color of the piece we are going to create.
     * @param initialLocation The location that the piece will be put in.
     */
    public Bishop(ChessBoard board, ChessPieceColor color, ChessPieceLocation initialLocation) {
        super(board, color, initialLocation);
        this.pieceID = color == ChessPieceColor.WHITE ? "wB" : "bB";
    }

    /**
     * This method computes, stores, and returns all of the legal moves the current piece can make given its exact location.
     *
     * @return Returns a list with all the possible moves that the current piece can make.
     */
    @Override
    public List<ChessPieceLocation> getPossibleMoves() {
        List<ChessPieceLocation> possibleMoves = new ArrayList<>();

        int currentColumn = getCurrentLocation().getColumn();
        int currentRow = getCurrentLocation().getRow();


        //Look at all the spaces diagonally up to the right
        ChessPieceLocation diagUpRight = new ChessPieceLocation(currentColumn + 1, currentRow + 1);
        while (diagUpRight.getColumn() <= 7 && diagUpRight.getRow() <= 7) {
            if (isValidMove(diagUpRight) != ChessMoveValidity.INVALID) {
                if (isValidMove(diagUpRight) == ChessMoveValidity.INVALID) {
                    break;
                }
                if (isValidMove(diagUpRight) == ChessMoveValidity.CAPTURE) {
                    possibleMoves.add(diagUpRight);
                    break;
                }
                possibleMoves.add(diagUpRight);
            } else if (isValidMove(diagUpRight) == ChessMoveValidity.INVALID) {
                break;
            }
            diagUpRight = new ChessPieceLocation(diagUpRight.getColumn() + 1, diagUpRight.getRow() + 1);
        }

        //Look at all the spaces diagonally up to the right
        ChessPieceLocation diagDownRight = new ChessPieceLocation(currentColumn + 1, currentRow - 1);
        while (diagDownRight.getColumn() <= 7 && diagDownRight.getRow() >= 0) {
            if (isValidMove(diagDownRight) != ChessMoveValidity.INVALID) {
                if (isValidMove(diagDownRight) == ChessMoveValidity.INVALID) {
                    break;
                }
                if (isValidMove(diagDownRight) == ChessMoveValidity.CAPTURE) {
                    possibleMoves.add(diagDownRight);
                    break;
                }
                possibleMoves.add(diagDownRight);
            } else if (isValidMove(diagDownRight) == ChessMoveValidity.INVALID) {
                break;
            }
            diagDownRight = new ChessPieceLocation(diagDownRight.getColumn() + 1, diagDownRight.getRow() - 1);
        }

        //Look at all the spaces diagonally up to the right
        ChessPieceLocation diagDownLeft = new ChessPieceLocation(currentColumn - 1, currentRow - 1);
        while (diagDownLeft.getColumn() >= 0 && diagDownLeft.getRow() >= 0) {
            if (isValidMove(diagDownLeft) != ChessMoveValidity.INVALID) {
                if (isValidMove(diagDownLeft) == ChessMoveValidity.INVALID) {
                    break;
                }
                if (isValidMove(diagDownLeft) == ChessMoveValidity.CAPTURE) {
                    possibleMoves.add(diagDownLeft);
                    break;
                }
                possibleMoves.add(diagDownLeft);
            } else if (isValidMove(diagDownLeft) == ChessMoveValidity.INVALID) {
                break;
            }
            diagDownLeft = new ChessPieceLocation(diagDownLeft.getColumn() - 1, diagDownLeft.getRow() - 1);
        }

        //Look at all the spaces diagonally up to the right
        ChessPieceLocation diagUpLeft = new ChessPieceLocation(currentColumn - 1, currentRow + 1);
        while (diagUpLeft.getColumn() >= 0 && diagUpLeft.getRow() <= 7) {
            if (isValidMove(diagUpLeft) != ChessMoveValidity.INVALID) {

                if (isValidMove(diagUpLeft) == ChessMoveValidity.CAPTURE) {
                    possibleMoves.add(diagUpLeft);
                    break;
                }
                possibleMoves.add(diagUpLeft);
            } else if (isValidMove(diagUpLeft) == ChessMoveValidity.INVALID) {
                break;
            }
            diagUpLeft = new ChessPieceLocation(diagUpLeft.getColumn() - 1, diagUpLeft.getRow() + 1);
        }


        return possibleMoves;
    }

    /**
     * This method will return the id of the current piece.
     *
     * @return The id of the current piece.
     */
    @Override
    public String getPieceID() {
        return this.pieceID;
    }
}
