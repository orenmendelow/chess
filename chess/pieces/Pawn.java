package chess.pieces;

import chess.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class extends the abstract {@link ChessPiece} class to define and implement a Pawn chess piece.
 *
 * @author Micky Aflalo
 * @author Oren Mendelow
 */
public class Pawn extends ChessPiece {

    /**
     * The id of the current piece.
     */
    private final String pieceID;

    /**
     * The field that determines if the current Pawn is able to be captured via the in passant move.
     */
    private boolean enpassantable;

    /**
     * Constructor that creates a new Pawn chess piece.
     *
     * @param board           The board that we are going to put the piece on.
     * @param color           The color of the piece we are going to create.
     * @param initialLocation The location that the piece will be put in.
     */
    public Pawn(ChessBoard board, ChessPieceColor color, ChessPieceLocation initialLocation) {
        super(board, color, initialLocation);
        this.pieceID = color == ChessPieceColor.WHITE ? "wp" : "bp";
    }

    /**
     * This method computes, stores, and returns all of the legal moves the current piece can make given its exact location.
     *
     * @return Returns a list with all the possible moves that the current piece can make.
     */
    @Override
    public List<ChessPieceLocation> getPossibleMoves() {
        List<ChessPieceLocation> possibleMoves = new ArrayList<>();

        int currentColumn = getCurrentLocation().getColumn();
        int currentRow = getCurrentLocation().getRow();


        int direction = getColor() == ChessPieceColor.WHITE ? 1 : -1;
        ChessPieceLocation forward = new ChessPieceLocation(currentColumn, currentRow + direction);

        if (isValidMove(forward) != ChessMoveValidity.INVALID) {
            possibleMoves.add(forward);
            if (!this.hasBeenMoved && isValidMove(forward) == ChessMoveValidity.EMPTY) {

                ChessPieceLocation forwardTwo = new ChessPieceLocation(currentColumn, currentRow + (direction * 2));

                if (isValidMove(forwardTwo) != ChessMoveValidity.INVALID) {
                    possibleMoves.add(forwardTwo);
                }
            }
        }

        ChessPieceLocation forwardRight = new ChessPieceLocation(currentColumn + direction, currentRow + direction);
        if (isValidMove(forwardRight) == ChessMoveValidity.CAPTURE) {
            possibleMoves.add(forwardRight);
        }

        ChessPieceLocation forwardLeft = new ChessPieceLocation(currentColumn - direction, currentRow + direction);
        if (isValidMove(forwardLeft) == ChessMoveValidity.CAPTURE) {
            possibleMoves.add(forwardLeft);
        }

        //Enpassant checks
        if ((this.getCurrentLocation().getRow() == 4 && this.getColor() == ChessPieceColor.WHITE) || (this.getCurrentLocation().getRow() == 3 && this.getColor() == ChessPieceColor.BLACK)) {

            ChessPieceLocation lookRight = new ChessPieceLocation(currentColumn + 1, currentRow);
            ChessPieceLocation lookLeft = new ChessPieceLocation(currentColumn - 1, currentRow);

            //White up right
            ChessPieceLocation enpassantUpRight = new ChessPieceLocation(currentColumn + 1, currentRow + 1);
            if (enpassantCheck(lookRight, enpassantUpRight)) {
                //System.out.println("[PawnEC] " + ChessBoard.publicGetPieceAtLocation(lookRight).getPieceID() + " at location: " + ChessBoard.publicGetPieceAtLocation(lookRight).getCurrentLocation() + " is enpassant-able!");
                possibleMoves.add(enpassantUpRight);
            }

            //White up left
            ChessPieceLocation enpassantUpLeft = new ChessPieceLocation(currentColumn - 1, currentRow + 1);
            if (enpassantCheck(lookLeft, enpassantUpLeft)) {
                //System.out.println("[PawnEC] " + ChessBoard.publicGetPieceAtLocation(lookLeft).getPieceID() + " at location: " + ChessBoard.publicGetPieceAtLocation(lookLeft).getCurrentLocation() + " is enpassant-able!");
                possibleMoves.add(enpassantUpLeft);
            }

            //Black Down right
            ChessPieceLocation enpassantDownRight = new ChessPieceLocation(currentColumn + 1, currentRow - 1);
            if (enpassantCheck(lookRight, enpassantDownRight)) {
                //System.out.println("[PawnEC] " + ChessBoard.publicGetPieceAtLocation(lookRight).getPieceID() + " at location: " + ChessBoard.publicGetPieceAtLocation(lookRight).getCurrentLocation() + " is enpassant-able!");
                possibleMoves.add(enpassantDownRight);
            }

            //Black Down Left
            ChessPieceLocation enpassantDownLeft = new ChessPieceLocation(currentColumn - 1, currentRow - 1);
            if (enpassantCheck(lookLeft, enpassantDownLeft)) {
                //System.out.println("[PawnEC] " + ChessBoard.publicGetPieceAtLocation(lookLeft).getPieceID() + " at location: " + ChessBoard.publicGetPieceAtLocation(lookLeft).getCurrentLocation() + " is enpassant-able!");
                possibleMoves.add(enpassantDownLeft);
            }


        }


        return possibleMoves;
    }

    /**
     * Sets the enpassantable flag to true on current Pawn.
     * This enables the current Pawn from being captured via the en passant move.
     */
    public void setEnpassantable() {
        this.enpassantable = true;
    }

    /**
     * Sets the enpassantable flag to false on current Pawn.
     * This disables the current Pawn from being captured via the en passant move.
     */
    public void unsetEnpassantable() {
        this.enpassantable = false;
    }


    /**
     * This method returns the state of the enpassantable flag of the current Pawn.
     * This tells the caller if the current Pawn can/cannot be captured via the en passat move.
     *
     * @return Returns the current state of the enpassantable flag of the current flag.
     */
    public boolean isEnpassantable() {
        return this.enpassantable;
    }

    /**
     * This method will determine if the pawn to the right or left of the current pawn is able to be captured via the en passant move
     * and if the location that the pawn will move to after the jump is valid.
     *
     * @param look   The location of the pawn to look at. Will either be right or left.
     * @param moveTo The location that the pawn will move to.
     * @return Returns true if the pawn to the right or left of the current pawn can be captured via the en passant move.
     */
    private boolean enpassantCheck(ChessPieceLocation look, ChessPieceLocation moveTo) {
        return isValidMove(look) == ChessMoveValidity.CAPTURE
                && ChessBoard.getPieceAtLocation(look).getPieceID().charAt(1) == 'p'
                && ((Pawn) ChessBoard.getPieceAtLocation(look)).isEnpassantable()
                && isValidMove(moveTo) != ChessMoveValidity.INVALID;
    }

    /**
     * This method will return the id of the current piece.
     *
     * @return The id of the current piece.
     */
    @Override
    public String getPieceID() {
        return this.pieceID;
    }

}
