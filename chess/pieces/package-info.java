/**
 * Contains the classes that implement each of the pieces within the game of Chess.
 * @author Micky Aflalo
 * @author Oren Mendelow
 */
package chess.pieces;