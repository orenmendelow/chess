package chess.pieces;

import chess.*;

import java.util.ArrayList;
import java.util.List;

/**
 * This class extends the abstract {@link ChessPiece} class to define and implement a King chess piece.
 *
 * @author Micky Aflalo
 * @author Oren Mendelow
 */
public class King extends ChessPiece {

    /**
     * The id of the current piece.
     */
    private final String pieceID;

    /**
     * Constructor that creates a new King chess piece.
     *
     * @param board           The board that we are going to put the piece on.
     * @param color           The color of the piece we are going to create.
     * @param initialLocation The location that the piece will be put in.
     */
    public King(ChessBoard board, ChessPieceColor color, ChessPieceLocation initialLocation) {
        super(board, color, initialLocation);
        this.pieceID = color == ChessPieceColor.WHITE ? "wK" : "bK";
    }

    /**
     * This method computes, stores, and returns all of the legal moves the current piece can make given its exact location.
     *
     * @return Returns a list with all the possible moves that the current piece can make.
     */
    @Override
    public List<ChessPieceLocation> getPossibleMoves() {

        List<ChessPieceLocation> possibleMoves = new ArrayList<>();

        int currentColumn = getCurrentLocation().getColumn();
        int currentRow = getCurrentLocation().getRow();

        //Can I move to the RIGHT from where I am at?
        ChessPieceLocation moveRight = new ChessPieceLocation(currentColumn + 1, currentRow);
        if (isValidMove(moveRight) != ChessMoveValidity.INVALID) {
            possibleMoves.add(moveRight);
        }

        //Can I move to the LEFT from where I am at?

        ChessPieceLocation moveLeft = new ChessPieceLocation(currentColumn - 1, currentRow);
        if (isValidMove(moveLeft) != ChessMoveValidity.INVALID) {
            possibleMoves.add(moveLeft);
        }

        //Can I move to the UP from where I am at?
        ChessPieceLocation moveUp = new ChessPieceLocation(currentColumn, currentRow + 1);
        if (isValidMove(moveUp) != ChessMoveValidity.INVALID) {
            possibleMoves.add(moveUp);
        }

        //Can I move to the Down from where I am at?
        ChessPieceLocation moveDown = new ChessPieceLocation(currentColumn, currentRow - 1);
        if (isValidMove(moveDown) != ChessMoveValidity.INVALID) {
            possibleMoves.add(moveDown);
        }

        //Can I move to Diagonally Up to the right from where I am at?
        ChessPieceLocation moveDiagUpRight = new ChessPieceLocation(currentColumn + 1, currentRow + 1);
        if (isValidMove(moveDiagUpRight) != ChessMoveValidity.INVALID) {
            possibleMoves.add(moveDiagUpRight);
        }

        //Can I move to Diagonally Up to the Left from where I am at?
        ChessPieceLocation moveDiagUpLeft = new ChessPieceLocation(currentColumn - 1, currentRow + 1);
        if (isValidMove(moveDiagUpLeft) != ChessMoveValidity.INVALID) {
            possibleMoves.add(moveDiagUpLeft);
        }

        //Can I move to Diagonally Down to the Left from where I am at?
        ChessPieceLocation moveDiagDownLeft = new ChessPieceLocation(currentColumn - 1, currentRow - 1);
        if (isValidMove(moveDiagDownLeft) != ChessMoveValidity.INVALID) {
            possibleMoves.add(moveDiagDownLeft);
        }

        //Can I move to Diagonally Down to the Right from where I am at?
        ChessPieceLocation moveDiagDownRight = new ChessPieceLocation(currentColumn + 1, currentRow - 1);
        if (isValidMove(moveDiagDownRight) != ChessMoveValidity.INVALID) {
            possibleMoves.add(moveDiagDownRight);
        }

        return possibleMoves;
    }

    /**
     * This method will return the id of the current piece.
     *
     * @return The id of the current piece.
     */
    @Override
    public String getPieceID() {
        return this.pieceID;
    }
}
