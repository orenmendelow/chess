package chess;


/**
 * The location contained within {@link ChessPiece} that specifies where on the {@link ChessBoard} it resides.
 *
 * @author Micky Aflalo
 * @author Oren Oren Mendelow
 */
public class ChessPieceLocation {

    /**
     * The column that the current chess piece is located on.
     */
    private final int column;

    /**
     * The row that the current chess piece is located on.
     */
    private final int row;

    /**
     * The constructor for a ChessPieceLocation object. Defines the column and row of the chess piece.
     *
     * @param column The column on the board
     * @param row    The row on the board
     */
    public ChessPieceLocation(int column, int row) {
        this.column = column;
        this.row = row;
    }

    /**
     * Returns the column of the current ChessPieceLocation object
     *
     * @return Returns the column of the current ChessPieceLocation object.
     */
    public int getColumn() {
        return column;
    }

    /**
     * Returns the row of the current ChessPieceLocation object
     *
     * @return Returns the row of the current ChessPieceLocation object.
     */
    public int getRow() {
        return row;
    }

    /**
     * This method will determine if the location is contained on the {@link ChessBoard}
     *
     * @return Returns true whether the location falls on the board, false if not.
     */
    public boolean isValid() {
        return this.column >= 0
                && this.column < ChessBoard.NUM_COLS
                && this.row >= 0
                && this.row < ChessBoard.NUM_ROWS;
    }

    /**
     * This method will print the current column and row of a ChessPieceLocation object.
     *
     * @return Returns the column and row of the current ChessPieceLocation object as a String.
     */
    public String toString() {
        return this.column + "," + this.row;
    }

    /**
     * This method takes a location on the board in the form of a string and converts it into a ChessPieceLocation object.
     *
     * @param location A string location coded as a chess location, such as e5
     * @return Returns a ChessPieceLocation object that is equivalent to the input.
     */
    public static ChessPieceLocation convertString(String location) {
        int column = Character.getNumericValue(location.charAt(0)) - 10;
        int row = Character.getNumericValue(location.charAt(1)) - 1;
        return new ChessPieceLocation(column, row);
    }

    /**
     * This method determines if two ChessPieceLocation objects are pointin to the same location.
     *
     * @param o The object to compare to the current object.
     * @return Returns true if both ChessPieceLocation objects are pointing to the same location, false if not.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChessPieceLocation that = (ChessPieceLocation) o;
        return column == that.column &&
                row == that.row;
    }

}
