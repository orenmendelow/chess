package chess;

import java.util.Scanner;

/**
 * This class is the main driver for the entire Chess application. It takes the user input and updates the board accordingly.
 * Ending the game is also handled here in the case of Draw, Resign, and Checkmate.
 *
 * @author Micky Aflalo
 * @author Oren Mendelow
 */
public class Chess {

    /**
     * This field: colorOfMove, alternates to tell the program whose turn it is and aids in certain color-dependent functions
     * which require which player currently has the option of making a move.
     */
    protected static ChessPieceColor colorOfMove = ChessPieceColor.WHITE;

    /**
     * This field: board, is the ChessBoard object on which the game is played and the {@link ChessPiece}'s are stored and updated.
     */
    private static ChessBoard board = new ChessBoard();

    /**
     * This method takes input from the user via standard-in (the command line) and calls the proper functions that
     * turn the string into an object that the program can interpret
     * takeUserInput also calls the functions that tell the main method whether or not the game has concluded
     *
     * @return Returns true if the input was correct, letting the game continue and false if the input was draw/Resign/Checkmate. Also reprompts the user if the input was incorrectly formatted.
     */
    public static boolean takeUserInput() {
        String whoseMove = colorOfMove == ChessPieceColor.WHITE ? "White's" : "Black's";
        System.out.print(whoseMove + " move: ");
        Scanner scan = new Scanner(System.in);
        String userInput = scan.nextLine();
        if (userInput.toLowerCase().equals("resign")) {
            System.out.println(colorOfMove == ChessPieceColor.WHITE ? "Black wins" : "White wins");
            return false;
        }
        if (userInput.toLowerCase().equals("draw")) {
            return false;
        }
        if (!board.move(userInput)) {
            System.out.println("Illegal move, try again");
            return takeUserInput();
        }
        // check for check mate
        if (board.isCheckMate()) {
            System.out.println("Checkmate");
            System.out.println(colorOfMove == ChessPieceColor.WHITE ? "Black wins" : "White wins");
            return false;
        }
        // check for check
        if (board.isColorInCheck()) {
            System.out.println("Check");
        }
        return true;
    }

    /**
     * This main method continuously calls the function takeUserInput() to take in user input and print the board
     * for as long as the user input is correct or the game ends.
     *
     * @param args Not used.
     */
    public static void main(String[] args) {
        board.printBoard();
        while (takeUserInput())
            board.printBoard();
    }
}
