/**
 * Contains the classes that implement the game of Chess with two players.
 * @author Micky Aflalo
 * @author Oren Mendelow
 */
package chess;