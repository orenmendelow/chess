package chess;

/**
 * The enums that define the validity of moves a chess piece can make.
 *
 * @author Micky Aflalo
 * @author Oren Mendelow
 */
public enum ChessMoveValidity {
    /**
     * The enum that specifies that a move was invalid.
     */
    INVALID,

    /**
     * The enum that specifies if the move was to an empty location.
     */
    EMPTY,

    /**
     * The enum that specifies if the move results in capturing an opposing players chess piece.
     */
    CAPTURE
}
