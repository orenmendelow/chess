package chess;

import chess.pieces.*;

import java.util.List;

import static chess.Chess.colorOfMove;

/**
 * The ChessBoard that the game of chess is played on and where the {@link ChessPiece}'s are stored and updated.
 *
 * @author Micky Aflalo
 * @author Oren Oren Mendelow
 */
public class ChessBoard {
    /**
     * NUM_ROWS and NUM_COLS pertain to the size of the chess board and exist to aid in code readability, as opposed to
     * using the seemingly meaningless value of '8' everywhere.
     */
    final static int NUM_ROWS = 8;
    final static int NUM_COLS = 8;

    /**
     * locationOfWhiteKing and locationOfBlackKing keep track of each team's King's location in order to check whether any piece
     * of the opposing color can capture it, called in getColorInCheck().
     */
    private ChessPieceLocation locationOfWhiteKing;
    private ChessPieceLocation locationOfBlackKing;

    /**
     * colorInCheck is used to keep track of which color is currently in check.
     */
    private ChessPieceColor colorInCheck;

    /**
     * board is the 2d matrix of ChessPiece objects that make up the actual board object on which the game is played.
     */
    private static ChessPiece[][] board = new ChessPiece[NUM_COLS][NUM_ROWS];

    /**
     * Constructor for the ChessBoard object, which initializes each location in the matrix to null and then
     * calls init() which populates the board with the proper pieces in their designated locations.
     */
    public ChessBoard() {
        for (int col = 0; col < ChessBoard.NUM_COLS; col++) {
            for (int row = 0; row < ChessBoard.NUM_ROWS; row++) {
                board[col][row] = null;
            }
        }
        init();
    }

    /**
     * This method is accessed by the main Chess class in order to print to the console and notify the users of a check.
     *
     * @return Returns the value of colorInCheck, which holds whether black, white, or nobody is currently in check
     */
    public boolean isColorInCheck() {
        return colorInCheck != null;
    }

    /**
     * This method prints the board to standard-out (the command line/console).
     */
    public void printBoard() {
        System.out.println();
        for (int row = ChessBoard.NUM_ROWS - 1; row >= 0; row--) {
            for (int col = 0; col < ChessBoard.NUM_COLS; col++) {
                ChessPieceLocation currLocation = new ChessPieceLocation(col, row);
                ChessPiece currPiece = getPieceAtLocation(currLocation);
                if (currPiece != null) {
                    System.out.print(currPiece.getPieceID());
                } else {
                    System.out.print((row - (col % 2)) % 2 == 0 ? "##" : "  ");
                }
                System.out.print(" ");
            }
            System.out.println(row + 1);
        }
        System.out.println(" a  b  c  d  e  f  g  h");
        System.out.println();
    }

    /**
     * This method which is called by the constructor, initializes the board with the proper pieces in their
     * respective locations and also initializes global variables locationOfWhiteKing and locationOfBlackKing.
     */
    // private initialize board
    private void init() {
        // pawn
        for (int i = 0; i < ChessBoard.NUM_COLS; i++) {
            int whitePawnRow = 1;
            int blackPawnRow = 6;
            board[i][whitePawnRow] = new Pawn(this, ChessPieceColor.WHITE, new ChessPieceLocation(i, whitePawnRow));
            board[i][blackPawnRow] = new Pawn(this, ChessPieceColor.BLACK, new ChessPieceLocation(i, blackPawnRow));
        }

        // rook
        for (int i = 0; i < 8; i += 7) {
            board[i][0] = new Rook(this, ChessPieceColor.WHITE, new ChessPieceLocation(i, 0));
            board[i][7] = new Rook(this, ChessPieceColor.BLACK, new ChessPieceLocation(i, 7));
        }

        // knight
        for (int i = 1; i < 7; i += 5) {
            board[i][0] = new Knight(this, ChessPieceColor.WHITE, new ChessPieceLocation(i, 0));
            board[i][7] = new Knight(this, ChessPieceColor.BLACK, new ChessPieceLocation(i, 7));
        }

        // bishop
        board[2][0] = new Bishop(this, ChessPieceColor.WHITE, new ChessPieceLocation(2, 0));
        board[2][7] = new Bishop(this, ChessPieceColor.BLACK, new ChessPieceLocation(2, 7));
        board[5][0] = new Bishop(this, ChessPieceColor.WHITE, new ChessPieceLocation(5, 0));
        board[5][7] = new Bishop(this, ChessPieceColor.BLACK, new ChessPieceLocation(5, 7));

        // queen
        board[3][0] = new Queen(this, ChessPieceColor.WHITE, new ChessPieceLocation(3, 0));
        board[3][7] = new Queen(this, ChessPieceColor.BLACK, new ChessPieceLocation(3, 7));

        // king
        board[4][0] = new King(this, ChessPieceColor.WHITE, new ChessPieceLocation(4, 0));
        locationOfWhiteKing = board[4][0].getCurrentLocation();
        board[4][7] = new King(this, ChessPieceColor.BLACK, new ChessPieceLocation(4, 7));
        locationOfBlackKing = board[4][7].getCurrentLocation();
    }

    /**
     * This method returns the chess piece at the specified location from the chessboard.
     *
     * @param location A location of a Piece on the ChessBoard object.
     * @return Returns the Piece from the ChessBoard at the specified location.
     */
    public static ChessPiece getPieceAtLocation(ChessPieceLocation location) {
        if (location == null) {
            return null;
        }
        return board[location.getColumn()][location.getRow()];
    }

    /**
     * This method checks whether the player black or white are in check by iterating through all pieces on the board and seeing
     * if any of them can capture the king.
     *
     * @return Returns the color of the player in check. Returns null if no player is in check.
     */
    private ChessPieceColor getColorInCheck() {
        for (ChessPiece[] row : board) {
            for (ChessPiece piece : row) {
                if (piece != null) {
                    for (ChessPieceLocation location : piece.getPossibleMoves()) {
                        if (piece.getColor() == ChessPieceColor.WHITE) {
                            if (location.equals(locationOfBlackKing)) {
                                // white is putting black in check
                                return ChessPieceColor.BLACK;
                            }
                        } else {
                            if (location.equals(locationOfWhiteKing)) {
                                // black is putting white in check
                                return ChessPieceColor.WHITE;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * This methods promotes a pawn to either a Knight, Rook, Bishop, or (default) Queen when they reach the opposite edge
     * of the board. It first checks whether the user specified which piece they'd like to promote into and then uses
     * a switch-case block to execute the promotion.
     *
     * @param input     The user's input which may contain which piece they'd like to promote to.
     * @param userPiece the piece at the location the user is looking to move
     */
    private void promote(String input, ChessPiece userPiece) {
        ChessPieceLocation userLocation = userPiece.getCurrentLocation();
        ChessPieceColor userColor = userPiece.getColor();
        // default promotion is queen
        char promoteToPiece = 'Q';
        if (input.length() >= 7) {
            promoteToPiece = input.charAt(6);
        }
        switch (promoteToPiece) {
            case 'N':
                board[userLocation.getColumn()][userLocation.getRow()] = new Knight(this, userColor, userLocation);
                return;
            case 'R':
                board[userLocation.getColumn()][userLocation.getRow()] = new Rook(this, userColor, userLocation);
                return;
            case 'B':
                board[userLocation.getColumn()][userLocation.getRow()] = new Bishop(this, userColor, userLocation);
                return;
            default:
                board[userLocation.getColumn()][userLocation.getRow()] = new Queen(this, userColor, userLocation);
        }
    }

    /**
     * This method checks castling syntax, ensures that neither the king nor rook involved have moved at any prior point
     * during the game, checks that all spaces between the king and rook are empty, and that no pieces over which the
     * king travels are under attack by the enemy.
     *
     * @param userPiece        the king ChessPiece
     * @param userMoveLocation the ChessPieceLocation where the king ends up after castling
     * @return Returns whether the king was able to castle.
     */
    private boolean canCastle(ChessPiece userPiece, ChessPieceLocation userMoveLocation) {
        // make sure we're not in check
        if (colorInCheck == userPiece.getColor()) {
            return false;
        }
        ChessPieceLocation rookLocation;
        ChessPieceLocation currKingLocation = userPiece.getColor() == ChessPieceColor.WHITE ? locationOfWhiteKing : locationOfBlackKing;
        int moveDistance = userMoveLocation.getColumn() - userPiece.getCurrentLocation().getColumn();
        int castlingDirection;
        // make sure userPiece hasn't been moved, and moveLocation is on the same row and 2 columns away
        if (!userPiece.hasBeenMoved
                && userPiece.getCurrentLocation().getRow() == userMoveLocation.getRow()
                && Math.abs(moveDistance) == 2) {
            if (moveDistance == 2) {
                // castling king side
                rookLocation = new ChessPieceLocation(7, userMoveLocation.getRow());
                castlingDirection = 1;
            } else {
                // castling queen side
                rookLocation = new ChessPieceLocation(0, userMoveLocation.getRow());
                castlingDirection = -1;
            }
            // get piece at rookLocation
            ChessPiece rook = getPieceAtLocation(rookLocation);
            // make sure it's a valid piece
            if (rook != null) {
                // make sure it's a rook and hasn't moved
                if (rook.getPieceID().charAt(1) == 'R' && !rook.hasBeenMoved) {
                    // make sure pieces in between king and rook are empty and king doesn't pass over any spaces under attack
                    ChessPieceLocation currPieceLocation = new ChessPieceLocation(4 + castlingDirection, userMoveLocation.getRow());
                    while (!currPieceLocation.equals(rookLocation)) {
                        // check if location is empty
                        if (getPieceAtLocation(currPieceLocation) != null) {
                            return false;
                        }
                        // check that the king doesn't pass over any spaces under attack
                        if (Math.abs(userPiece.getCurrentLocation().getColumn() - currPieceLocation.getColumn()) <= 2) {
                            if (userPiece.getColor() == ChessPieceColor.WHITE) {
                                locationOfWhiteKing = currPieceLocation;
                                if (getColorInCheck() == userPiece.getColor()) {
                                    locationOfWhiteKing = currKingLocation;
                                    return false;
                                }
                            } else {
                                locationOfBlackKing = currPieceLocation;
                                if (getColorInCheck() == userPiece.getColor()) {
                                    locationOfBlackKing = currKingLocation;
                                    return false;
                                }
                            }
                        }
                        currPieceLocation = new ChessPieceLocation(currPieceLocation.getColumn() + castlingDirection, currPieceLocation.getRow());
                    }
                    // move pieces and update global king location
                    movePiece(userPiece, userMoveLocation);
                    userPiece.hasBeenMoved = true;
                    movePiece(rook, new ChessPieceLocation(userPiece.getCurrentLocation().getColumn() - (moveDistance / 2), userMoveLocation.getRow()));
                    rook.hasBeenMoved = true;
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * This method checks whether an existing check is in fact a check mate. It does this by analyzing whether any
     * move by the team under check would escape the check. If not, it is invariably check mate.
     *
     * @return True if the current player is in checkmate. False if they are not.
     */
    public boolean isCheckMate() {
        if (getColorInCheck() == colorOfMove) {
            for (ChessPiece[] column : board) {
                for (ChessPiece piece : column) {
                    // see if piece exists
                    if (piece != null) {
                        // see if it's our color piece
                        if (piece.getColor() == colorOfMove) {
                            // check if any of possible moves can take king out of check
                            ChessPieceLocation initialPieceLocation = piece.getCurrentLocation();
                            for (ChessPieceLocation location : piece.getPossibleMoves()) {
                                ChessPiece pieceAtMoveLocation = getPieceAtLocation(location);
                                // see if moving piece to this location will eliminate check
                                movePiece(piece, location);
                                if (getColorInCheck() != colorOfMove) {
                                    // check can be avoided, put piece in initial location and return piece that was there
                                    movePiece(piece, initialPieceLocation);
                                    board[location.getColumn()][location.getRow()] = pieceAtMoveLocation;
                                    return false;
                                }
                                // put pieces back
                                movePiece(piece, initialPieceLocation);
                                board[location.getColumn()][location.getRow()] = pieceAtMoveLocation;
                            }
                        }
                    }
                }
            }
            // no pieces could eliminate check, must be check mate
            return true;
        }
        return false;
    }

    /**
     * This method calls the function that confirms proper syntax, makes sure the user is trying to execute a valid move on
     * a valid piece, and then executes the proper move, whether it be a standard move, castle, en passant, or promotion.
     *
     * @param input the user's input, a string
     * @return Returns true if the move was valid/successful or false if not.
     */
    public boolean move(String input) {
        if (hasValidInputSyntax(input)) {
            //If we got here, then we have valid syntax, its just a matter if the move itself is valid.
            // Thus we need to reset the enpassant-able boolean for each of current colors pawns
            enpassantReset();
            List<ChessPieceLocation> inputLocations = convertStringToLocations(input.substring(0, 5));
            ChessPieceLocation userPieceLocation = inputLocations.get(0);
            ChessPieceLocation userMoveLocation = inputLocations.get(1);
            ChessPiece userPiece = getPieceAtLocation(userPieceLocation);
            // make sure piece exists at location
            if (userPiece == null) {
                return false;
            }
            // make sure we're moving a piece of our color
            if (userPiece.getColor() != colorOfMove) {
                return false;
            }
            ChessPiece pieceAtMoveLocation = getPieceAtLocation(userMoveLocation);
            if (isPossibleMove(userPiece, userMoveLocation)) {
                //Check for enpassant
                if (userPiece.getPieceID().charAt(1) == 'p') {
                    enpassantCheck(userPiece, userMoveLocation);
                    if ((userPiece.getCurrentLocation().getRow() == 4 && userPiece.getColor() == ChessPieceColor.WHITE) || (userPiece.getCurrentLocation().getRow() == 3 && userPiece.getColor() == ChessPieceColor.BLACK)) {
                        enpassantMove(userPiece, userMoveLocation);
                    }
                }
                // move the piece
                movePiece(userPiece, userMoveLocation);
                // check if move exposes king
                colorInCheck = getColorInCheck();
                if (colorInCheck == colorOfMove) {
                    movePiece(getPieceAtLocation(userMoveLocation), userPieceLocation);
                    board[userMoveLocation.getColumn()][userMoveLocation.getRow()] = pieceAtMoveLocation;
                    colorInCheck = getColorInCheck();
                    return false;
                }
                //check if pawn is at the end
                if (userPiece.getPieceID().charAt(1) == 'p'
                        && (userPiece.getCurrentLocation().getRow() == 0
                        || userPiece.getCurrentLocation().getRow() == 7)) {
                    promote(input, userPiece);
                }
                userPiece.hasBeenMoved = true;
                colorOfMove = colorOfMove == ChessPieceColor.WHITE ? ChessPieceColor.BLACK : ChessPieceColor.WHITE;
                return true;
            } else {
                // check for castle
                if (userPiece.getPieceID().charAt(1) == 'K') {
                    if (canCastle(userPiece, userMoveLocation)) {
                        colorInCheck = getColorInCheck();
                        colorOfMove = colorOfMove == ChessPieceColor.WHITE ? ChessPieceColor.BLACK : ChessPieceColor.WHITE;
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * This method executes the actual piece move on the board.
     *
     * @param userPiece        the piece being moved
     * @param userMoveLocation the location userPiece is moving to
     */
    private void movePiece(ChessPiece userPiece, ChessPieceLocation userMoveLocation) {
        //Clear the spot of the place we are about to move from
        board[userPiece.getCurrentLocation().getColumn()][userPiece.getCurrentLocation().getRow()] = null;
        //Move the piece to the new location
        board[userMoveLocation.getColumn()][userMoveLocation.getRow()] = userPiece;
        //Set piece to the new location
        userPiece.setCurrentLocation(userMoveLocation);
        //check if piece being moved is a king
        if (userPiece.getPieceID().charAt(1) == 'K') {
            if (userPiece.getColor() == ChessPieceColor.WHITE) {
                locationOfWhiteKing = userMoveLocation;
            } else {
                locationOfBlackKing = userMoveLocation;
            }
        }
    }

    /**
     * This method checks to see if the position that the current pawn will move to result
     * in allowing the opposing team to capture it via the en passat move.
     *
     * @param userPiece        The current piece that is about to be moved. This should never be anything but a pawn
     * @param userMoveLocation The position that the userPiece is going to move to
     */
    private void enpassantCheck(ChessPiece userPiece, ChessPieceLocation userMoveLocation) {

        //White
        if (userPiece.getColor() == ChessPieceColor.WHITE) {

            //Has the piece been moved yet?
            if (!userPiece.hasBeenMoved) {
//                System.out.println("[enpassantCheck] wp at location: " + userPiece.getCurrentLocation() + " was just moved for the first time!");
                userPiece.hasBeenMoved = true;

                //Is the piece about to do a double jump?
                if (userPiece.getCurrentLocation().getRow() + 2 == userMoveLocation.getRow()) {
//                    System.out.println("[enpassantCheck] wp at location: " + userPiece.getCurrentLocation() + " just did a double jump!");

                    //If we got here, then we can be captured by using the enpassant move
//                    System.out.println("[enpassantCheck] wp at location: " + userPiece.getCurrentLocation() + " is enpassant-able!");
                    ((Pawn) userPiece).setEnpassantable();
                    return;
                }
            }

//            System.out.println("[enpassantCheck] wp at location: " + userPiece.getCurrentLocation() + " is NOT enpassant-able!");
            ((Pawn) userPiece).unsetEnpassantable();
        }
        //its black
        else if (userPiece.getColor() == ChessPieceColor.BLACK) {
            //Has the piece been moved yet?
            if (!userPiece.hasBeenMoved) {
//                System.out.println("[enpassantCheck] bp at location: " + userPiece.getCurrentLocation() + " was just moved for the first time!");
                userPiece.hasBeenMoved = true;

                //Is the piece about to do a double jump?
                if (userPiece.getCurrentLocation().getRow() - 2 == userMoveLocation.getRow()) {
//                    System.out.println("[enpassantCheck] bp at location: " + userPiece.getCurrentLocation() + " just did a double jump!");

                    //If we got here, then we can be captured by using the enpassant move
//                    System.out.println("[enpassantCheck] bp at location: " + userPiece.getCurrentLocation() + " is enpassant-able!");
                    ((Pawn) userPiece).setEnpassantable();
                    return;
                }
            }
//            System.out.println("[enpassantCheck] bp at location: " + userPiece.getCurrentLocation() + " is NOT enpassant-able!");
            ((Pawn) userPiece).unsetEnpassantable();
        }
    }

    /**
     * This method will preform the en passant capture, removing the opposing teams pawn from the board.
     *
     * @param userPiece        The current piece that is about to be moved. This should never be anything but a pawn
     * @param userMoveLocation The position that the userPiece is going to move to
     */
    private void enpassantMove(ChessPiece userPiece, ChessPieceLocation userMoveLocation) {

        //Capture the piece to the right - white
        if (userPiece.getCurrentLocation().getRow() + 1 == userMoveLocation.getRow()
                && userPiece.getCurrentLocation().getColumn() + 1 == userMoveLocation.getColumn()) {
//            System.out.println("The move that is about to be preformed is an enpassant");
            //Since we just preformed an enpassant move, we have to capture the other pawn
            board[userPiece.getCurrentLocation().getColumn() + 1][userPiece.getCurrentLocation().getRow()] = null;
        }

        //Capture the piece to the left - white
        else if (userPiece.getCurrentLocation().getRow() + 1 == userMoveLocation.getRow()
                && userPiece.getCurrentLocation().getColumn() - 1 == userMoveLocation.getColumn()) {
//            System.out.println("The move that is about to be preformed is an enpassant");
            //Since we just preformed an enpassant move, we have to capture the other pawn
            board[userPiece.getCurrentLocation().getColumn() - 1][userPiece.getCurrentLocation().getRow()] = null;
        }

        //Capture the piece to the right - black
        else if (userPiece.getCurrentLocation().getRow() - 1 == userMoveLocation.getRow()
                && userPiece.getCurrentLocation().getColumn() + 1 == userMoveLocation.getColumn()) {
//            System.out.println("The move that is about to be preformed is an enpassant");
            //Since we just preformed an enpassant move, we have to capture the other pawn
            board[userPiece.getCurrentLocation().getColumn() + 1][userPiece.getCurrentLocation().getRow()] = null;
        }


        //Capture the piece to the left - black
        else if (userPiece.getCurrentLocation().getRow() - 1 == userMoveLocation.getRow()
                && userPiece.getCurrentLocation().getColumn() - 1 == userMoveLocation.getColumn()) {
//            System.out.println("The move that is about to be preformed is an enpassant");
            //Since we just preformed an enpassant move, we have to capture the other pawn
            board[userPiece.getCurrentLocation().getColumn() - 1][userPiece.getCurrentLocation().getRow()] = null;
        }
    }

    /**
     * This method turns off the field in the Pawn object that dictates if it can be captured via the en passant move.
     * This is necessary if the opposing team does not/cannot capture the vulnerable pawn during their turn.
     */
    private void enpassantReset() {
        //For each row of the board
        for (ChessPiece[] row : board) {
            //Lets look at all the pieces
            for (ChessPiece piece : row) {
                //If the piece is NOT null
                if (piece != null) {
                    //And the piece is a pawn that is the same as the color of the player about to move
                    if (piece.getPieceID().charAt(1) == 'p' && piece.getColor() == colorOfMove) {
                        //If the piece is enpassantable
                        if (((Pawn) piece).isEnpassantable()) {
                            //Make it not enpassantable
                            ((Pawn) piece).unsetEnpassantable();
//                            System.out.println("[ENReset] We just reset the enpassantability of "+((Pawn)piece).getPieceID()+" at location: "+((Pawn)piece).getCurrentLocation());
                        }
                    }
                }
            }
        }
    }

    /**
     * This method converts the user input into {@link ChessPieceLocation}'s. The piece to move and where to move the piece to.
     *
     * @param input the user's input, a string
     * @return Returns a list containing two ChessPieceLocation objects. The first is the location of the piece the user is trying to move,
     * the second the location to which the user is trying to move their piece.
     */
    private List<ChessPieceLocation> convertStringToLocations(String input) {
        ChessPieceLocation userPieceLocation = ChessPieceLocation.convertString(input.substring(0, 2));
        ChessPieceLocation userMoveLocation = ChessPieceLocation.convertString(input.substring(3, 5));
        return List.of(userPieceLocation, userMoveLocation);
    }

    /**
     * This method analyzes whether there is a piece at the given location, whether the piece is on the same team
     * as the current user whose turn it is, and lastly, checks whether the move is a valid move for the given piece.
     *
     * @param userPiece        the piece the user is trying to move
     * @param userMoveLocation the location to which the user is trying to move
     * @return Returns true if the move is possible, false if not.
     */
    private boolean isPossibleMove(ChessPiece userPiece, ChessPieceLocation userMoveLocation) {
        // check that there is a piece at given coordinate
        if (userPiece != null) {
            // check that piece color is same as turn color
            if (userPiece.getColor() == colorOfMove) {
                // obtain all legal moves for piece
                List<ChessPieceLocation> userPossibleMoves = userPiece.getPossibleMoves();
                // check that second coordinate is a legal move
                return userPossibleMoves.contains(userMoveLocation);
            }
        }
        return false;
    }

    /**
     * This method determines if the user input to move a piece adheres to our format.
     *
     * @param input the user's input, a string
     * @return Returns true whether the user's input matches the correct syntax, checked by comparing with regex. Returns false if not.
     */
    private static boolean hasValidInputSyntax(String input) {
        // check that syntax for move is correct
        return input.substring(0, 5).matches("\\S\\d \\S\\d");
    }
}
