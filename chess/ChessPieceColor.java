package chess;

/**
 * The enums that define the colors that a chess piece can be.
 *
 * @author Micky Aflalo
 * @author Oren Mendelow
 */
public enum ChessPieceColor {
    /**
     * This is the enum for the white player.
     */
    WHITE,

    /**
     * The enum for the black player.
     */
    BLACK
}
